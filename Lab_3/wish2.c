#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>

// <---------- BEGIN Redirection Fucntion ------------>
void redir(char *executable, char *fileName)
{
  int filefd = open(fileName, O_WRONLY | O_CREAT, 0666);
  if (!fork())
  {
    close(1);
    dup(filefd);
    execlp(executable, executable, NULL);
  }
  else
  {
    close(filefd);
    wait(NULL);
  }
}
// <---------- END Redirection Fucntion ------------>

void *myFun(void*  x)
{

  char *j = (char* ) x;
  char *array3[50];

  char *token;

  token = strtok(j," ");
  int z = 0;

  while(token != NULL){
    array3[z] = token;
    token = strtok(NULL, " "); 
    z++;
    printf("Token: %s\n", array3[z]);
  }
  
  int rl = fork();
  if (rl < 0)
  {
    // fork failed; exit
    fprintf(stderr, "fork failed\n");
  }

  else if (rl == 0)
  {
    // child (new process)
    execvp(array3[0], array3);
  }
  // Main (parent) process after fork succeeds
  else
  {
    int returnStatus;
    waitpid(rl, &returnStatus, 0); // Parent process waits here for child to terminate.

    if (returnStatus == 0) // Verify child process terminated without error.
    {
      //printf("The child process terminated normally.\n");
    }

    if (returnStatus == 1)
    {
      printf("The child process terminated with an error!.\n");
      exit(1);
    }
  }
  return NULL;

}

int main(int argc, char const *argv[]) {
  int sl = 0;
  char * pathz;

  while (1)
  {
    //Starts loop
    //If there is only 1 agrument parsed when initialising wish run in interactive mode.
    const char *exitt = "exit\n";
    int exitcheck = strcmp(argv[0], exitt);
    if (exitcheck != 0)
    {
      printf("wish> ");
      char *buff;
      size_t size = 200;
      //size_t characters;
      getline(&buff, &size, stdin); // Get line from standard input

      //  <------- Start Tokenisation --------->
      char search_string[300];
      strcpy(search_string, &buff[0]);
      char *array[50];
      char *array2[50];

      int loop;
      char *dest;
      int rd = 0;
      char *argss[50];
      int par = 0;
      

      // char search_par[50];


      array[0] = strtok(search_string, " \n\a\t\r");
      //printf("%s\n",array[0]);
      if (array[0] == NULL)
      {
        printf("No test to search.\n");
        exit(0);
      }

      for (loop = 1; loop < 50; loop++)
      {   
        array[loop] = strtok(NULL, " \n\a\t\r");
        

        if (array[loop] == NULL)
        {
          break;
          }
          else if(strcmp(">", array[loop]) == 0){
              printf("This is a redirection\n");
              rd = 1;
          }

          else if(rd == 1){
              dest = array[loop];
              printf("%s\n", dest);
          }

          if (strcmp("&", array[loop]) == 0)
          {
            printf("This is a parallel \n");
            par = 1;
          }
      }

      // <-------- Finished Tokenisation --------->

      // <--------- Start args identification -------->

      array2[0] = strtok(buff, "&");
      printf("array2[0]contains: %s \n", array2[0]);
      //printf("%s\n",array[0]);
      if (array2[0] == NULL)
      {
        printf("No test to search in array 2.\n");
        exit(0);
      }
      int argnum = 1;
      for (loop = 1; loop < 50; loop++)
      {
        array2[loop] = strtok(NULL, " & ");

        if (array2[loop] == NULL)
        {
          break;
        }
        else if(array2[loop]!= NULL){
          argnum++;
          printf("array2 contains: %s \n", array2[loop]);
        }
      }
        //<----------- Stop args identification ---------->

        if (sl == 0)
        {
          pathz = array[0];
          sl = 1;
        }

        if (array[0] != NULL )
        { //If read line is not empty run
          const char *exittt = "exit";
          int exitcheck2 = strcmp(array[0], exittt);

          //printf("%d\n", exitcheck);

          if (exitcheck2 != 0)
          { // If the user has not typed exit run this block of code

            //printf("%s\n", buff);

            const char *change = "cd";
            int cdcheck = strcmp(array[0], change);

            //printf("cdcheck\n");
            //printf("%d\n", cdcheck);

            //If the user has typed cd run this block of code
            if (cdcheck == 0 && array[1]!=NULL)
            {
              int ret;
              char path[200];
              char *newl[10];
              newl[0] = strtok(&buff[3], "\n");
              strcpy(path, newl[0]);

              ret = chdir(path);

              //printf("File Location\n");
              //printf("%s\n", path);

              if (ret == 0)
              {
                printf("You've changed directory\n");
                
                
              }

              else if (ret != 0)
              {
                printf("File path does not exist\n");
               
              }
            }
            
            // <------ End of change directory code ------>//

            // <----- If user enter path ------> 

            change = "path";
            int pathcheck = strcmp(array[0], change);
            int ret;
            

            if(pathcheck == 0 ){
              printf("You're changing the execution directory\n");
              printf("%s",array[1]);

              char * fpath = "/usr/";
              char * spath = "/usr/bin/";
              int fpathcheck = strcmp(array[1], fpath);
              int spathcheck = strcmp(array[1], spath);

              if(fpathcheck == 0 || spathcheck == 0){
                pathz = NULL;
              }

              else {
                pathz = array[1];
              }

              

              int rc = fork();
              if (rc < 0)
              {
                // fork failed; exit
                fprintf(stderr, "fork failed\n");
              }
              else if (rc == 0)
              {
                // child (new process)
                ret = chdir(array[1]);
                if (ret == 0){
                  printf("Directory changed\n");
                  execvp(pathz, array);
                }

                else if (ret !=0){
                  printf("Invalid directory input\n");
                }
              }
              else // Main (parent) process after fork succeeds
              {
                int returnStatus;
                waitpid(rc, &returnStatus, 0); // Parent process waits here for child to terminate.
                exit(0);
                }
            }
             // <-------- End of path code ------->
            // <-------- Begining of system function code ------->
            else if (cdcheck !=0 && pathcheck !=0){

              if (rd == 1 && par == 0){
                redir(array[0],dest);


              }
            // <--------- Running parallel code ---------->
              else if(par ==1){
                printf("Number of arguments is: %d \n", argnum);

                for (int x = 0;x<argnum;x++ ){
                  printf("Array2: %s\n", array2[x]);
                }
                
                pthread_t threads[argnum];
                int thread_args[argnum];


                

                /* spawn the threads */
                for (int i = 0; i <argnum; ++i)
                {
                  thread_args[i] = i;
                  printf("spawning thread %d\n", i);
                  pthread_create(&threads[i], NULL, myFun,array2[i]);
                }

                /* wait for threads to finish */
                for (int i = 0; i <argnum; ++i)
                {
                  pthread_join(threads[i], NULL);
                }
              }
            //<--------- Ending parallel code ------------->

              else if (rd==0 && par==0){
                int rc = fork();
                if (rc < 0){ 
                // fork failed; exit
                fprintf(stderr, "fork failed\n");
                }
                
                else if (rc == 0){ 
                // child (new process)
                execvp(pathz, array);
                }
                // Main (parent) process after fork succeeds
                else
                {
                  int returnStatus;
                  waitpid(rc, &returnStatus, 0); // Parent process waits here for child to terminate.

                  if (returnStatus == 0) // Verify child process terminated without error.
                  {
                    //printf("The child process terminated normally.\n");
                  }

                  if (returnStatus == 1)
                  {
                    printf("The child process terminated with an error!.\n");
                    exit(1);
                  }
                }
              }
            }
          }

          else if (exitcheck2 == 0)
          {
            printf("You've exit\n");
            exit(1);
          }
        } // End of exit check
        } // End of if first array is not equal to null

        else if (exitcheck ==0){
          exit(0);
        }

// <------------ BATCH MODE ---------------->>

        else if (argc == 2){ // If 2 parameters have been parsed when initialising wish run in batch mode 
          printf("You've entered batch mode\n");
          FILE *fp = fopen(argv[1], "r");
          if(fp != NULL){
            //printf("File is readable\n");
            while (1){
              char line[200];
              char *buffer = fgets(line, 70, fp); // Get line
              if (buffer==NULL){
                printf("Ended\n");
              }

              //  <------- Start Tokenisation --------->
              char search_string[300];
              strcpy(search_string, &buffer[0]);
              char *array[50];
              int loop;

              array[0] = strtok(search_string, " \n\a\t\r");
              if (array[0] == NULL)
              {
                printf("No test to search.\n");
                exit(0);
              }

              for (loop = 1; loop < 50; loop++)
              {
                array[loop] = strtok(NULL, " \n\a\t\r");
                if (array[loop] == NULL)
                  break;
              }

              // <-------- Finished Tokenisation --------->

              int rc = fork();
              if (rc < 0)
              {
                // fork failed; exit
                fprintf(stderr, "fork failed\n");
              }
              else if (rc == 0)
              {
                // child (new process)
                const char *change = "cd";
                int cdcheck = strcmp(array[0], change);

                //printf("%d\n",cdcheck);
                //printf("%s\n",array[0]);
                
                if (cdcheck==0){
                  chdir(array[1]);
                }
                else{
                execvp(pathz, array);
                }
              }
              else // Main (parent) process after fork succeeds
              {
                int returnStatus;
                waitpid(rc, &returnStatus, 0); // Parent process waits here for child to terminate.

                if (returnStatus == 0) // Verify child process terminated without error.
                {
                  //printf("The child process terminated normally.\n");
                  exit(0);
                }

                if (returnStatus == 1)
                {
                  printf("The child process terminated with an error!.\n");
                  exit(0);
                }
              }
            }
            exit(0);
          }
          else{
            printf("File does not exist or cannot be read.\n");
            exit(0);
            }
        }
    }
}

